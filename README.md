# Serverless - AWS Node.js Typescript

This project used [Serverless framework](https://www.serverless.com/) & Nodejs/Typescript to develop the simple product CRUD application.

## Installation/deployment instructions

Depending on your preferred package manager, follow the instructions below to deploy your project.

> **Requirements**: NodeJS & Java (For running dynamodb in local machine)

### Using NPM

- Run `npm i` to install the project dependencies
- Run `npm run setup:dynamodb` to install the dynamodb local dependencies
- Run `npm run setup:offline` to install the serverless offline dependencies
- Run `npm run setup:local` to setup the dynamodb local and serverless offline
- Run `npm run start:dynamodb` to start the dynamodb local in local machine
- Run `npm run start:offline` to start the serverless offline in local machine
- Run `npm run start:local` to start the dynamodb local and serverless offline in local machine
- Run `npm run deploy` to start the deploy serverless application to AWS Cloud

### API Endpoint Url

- Live
  - https://mwzf5g1jzg.execute-api.ap-southeast-1.amazonaws.com/dev
- Local
  - http://localhost:3000/dev

### Local API endpoints

- GET
  - http://localhost:3000/dev/product
- POST
  - http://localhost:3000/dev/product
    - Request Body
      - code: String
      - name: String
      - type: String
      - price: Number
      - status: Active or Suspend
- PUT
  - http://localhost:3000/dev/product/:id
    - :id
      - ProductId
    - Request Body
      - code: String
      - name: String
      - type: String
      - price: Number
      - status: Active or Suspend
- DELETE
  - http://localhost:3000/dev/product/:id
    - :id
      - ProductId

### Locally

Before test the product function locally, please create the .env file with the content of .env.example file.

In order to test the product function locally, run the following command:

- `npm run install:dynamodb` to install dynamodb local service
- `npm run install:offline` install serverless offline service
- `npm run start:dynamodb` to start dynamodb local service
- `npm run start:offline` start serverless offline service

Or you can run the following command to setup and install both serverless offline and dynamodb local

- `npm run setup:local` start both dynamodb local and serverless offline service
- `npm run start:local` start both dynamodb local and serverless offline service

### Dynamodb Local Issues (26/3/2023)

If you faced the issue when installing dynamodb local, please refer to this [solution](https://github.com/99x/serverless-dynamodb-local/issues/294#issuecomment-1460157435)

### Deploy to AWS Cloud

In order to deploy the serverless application to AWS, please refer to the [documentation](https://www.serverless.com/framework/docs/providers/aws/guide/credentials/)

After configured the AWS credential, You can also run the following command:

- `npm run deploy` to deploy the serverless application to your AWS Cloud

Or can use Serverless Framework CLI to deploy the application. For more information, please refer [documentation](https://www.serverless.com/framework/docs/providers/aws/guide/deploying)

### Project structure

The project code base is mainly located within the `src` folder. This folder is divided in:

```
.
├── src
│   ├── functions                 # Lambda configuration and source code folder
│   │   └── product
│   │       ├── handler.ts        # `Product` lambda source code
│   │       └── index.ts          # `Product` lambda Serverless configuration
│   │
│   ├── libs                      # Lambda shared code
│   │    ├─ apiGateway.ts         # API Gateway specific helpers
│   │    ├── dynamoDb.ts          # DynamoDb Endpoint Between local & AWS Cloud
│   │    ├── dynamoDbClient.ts    # DynamoDb Client helper function
│   │    ├── handlerResolver.ts   # Sharable library for resolving lambda handlers
│   │    ├── lambda.ts            # Lambda middleware & Request Body Validation
│   │    └── utilities.ts         # Sharable functions
│   │
│   ├── models                    # Data Model
│   │    ├── index.ts             # DynamoDb Instance Initialization
│   │    └── product.ts           # Product Interface
│   │
│   ├── services                  # Services
│   │    ├── index.ts             # Product Service and DynamoDb Client Initialization
│   │    └── product.ts           # Product Service Code
│   │
│   └── types                     # Types
│        ├── apigateway.ts        # Define Product POST/PUTRequest Body
│        └── response.ts          # Define the API Response type
│
├── package.json
├── serverless.ts                 # Serverless service file
├── tsconfig.json                 # Typescript compiler configuration
└── tsconfig.paths.json           # Typescript paths
```

### 3rd party libraries

- [json-schema-to-ts](https://github.com/ThomasAribart/json-schema-to-ts) - uses JSON-Schema definitions used by API Gateway for HTTP request validation to statically generate TypeScript types in your lambda's handler code base
- [middy](https://github.com/middyjs/middy) - middleware engine for Node.Js lambda. This template uses [http-json-body-parser](https://github.com/middyjs/middy/tree/master/packages/http-json-body-parser) to convert API Gateway `event.body` property, originally passed as a stringified JSON, to its corresponding parsed object
- [@serverless/typescript](https://github.com/serverless/typescript) - provides up-to-date TypeScript definitions for your `serverless.ts` service file

