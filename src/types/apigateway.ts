import { APIGatewayProxyEvent } from "aws-lambda";

type Modify<T, R> = Omit<T, keyof R> & R;

export interface APIGatewayProductBodyObject {
    code: string;
    name: string;
    type: string;
    price: number;
    status: 'Active'|'Suspend';
}

export type CustomAPIGatewayProxyEvent  = Modify<APIGatewayProxyEvent , {
    body: APIGatewayProductBodyObject
}>