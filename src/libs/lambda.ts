import middy from "opt/nodejs/@middy/core";
import middyJsonBodyParser from "opt/nodejs/@middy/http-json-body-parser";
import httpErrorHandler from 'opt/nodejs/@middy/http-error-handler';
import validator from 'opt/nodejs/@middy/validator';
import { transpileSchema } from 'opt/nodejs/@middy/validator/transpile';
import { formatJSONResponse } from './apiGateway';
import { Response } from "../../layers/product/src/types/response";

export const middyfy = (handler) => {
  return middy(handler)
    .use(middyJsonBodyParser())
}

export const middyfyWithUpsert = (eventSchema, handler) => {
  return middy(handler)
    .use(middyJsonBodyParser())
    .use(validator({ eventSchema: transpileSchema(eventSchema) }))
    .use({
      onError: (request) => {
        const error = <any>request.error;
        if (error.statusCode != 400) return;
        if (!error.expose || !error.cause) return;
        return formatJSONResponse(<Response>{
          status: 500,
          data: {},
          message: error.cause
        });
      },
    })
    .use(httpErrorHandler())
}


