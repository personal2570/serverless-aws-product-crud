import { handlerPath } from 'src/libs/handlerResolver';

export const createProduct = {
    handler: `${handlerPath(__dirname)}/handler.createProduct`,
    layers: [
        { Ref: "ProductLambdaLayer" },
        { Ref: "NodeLambdaLayer" },
    ],
    events: [
        {
            http: {
                method: 'POST',
                path: 'product',
            },
        },
    ],
};

export const getAllProducts = {
    handler: `${handlerPath(__dirname)}/handler.getAllProducts`,
    layers: [
        { Ref: "ProductLambdaLayer" },
        { Ref: "NodeLambdaLayer" },
    ],
    events: [
        {
            http: {
                method: 'GET',
                path: 'product',
            },
        },
    ],
};

export const updateProduct = {
    handler: `${handlerPath(__dirname)}/handler.updateProduct`,
    layers: [
        { Ref: "ProductLambdaLayer" },
        { Ref: "NodeLambdaLayer" },
    ],
    events: [
        {
            http: {
                method: 'PUT',
                path: 'product/{id}',
            },
        },
    ],
};

export const deleteProduct = {
    handler: `${handlerPath(__dirname)}/handler.deleteProduct`,
    layers: [
        { Ref: "ProductLambdaLayer" },
        { Ref: "NodeLambdaLayer" },
    ],
    events: [
        {
            http: {
                method: 'DELETE',
                path: 'product/{id}',
            },
        },
    ],
};