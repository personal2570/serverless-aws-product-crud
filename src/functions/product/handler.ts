import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";
import { v4 } from "opt/nodejs/uuid";
import { middyfy, middyfyWithUpsert } from '@libs/lambda';
import { formatJSONResponse } from '@libs/apiGateway';
import { getErrorMessage } from 'opt/product/src/libs/utilities';
import { productSchema } from "opt/product/src/models/product";
import { CustomAPIGatewayProxyEvent, APIGatewayProductBodyObject } from "@types/apiGateway";
import { Response } from "opt/product/src/types/response";
import productService from 'opt/product/src/services/index';

export const createProduct = middyfyWithUpsert(productSchema, async (event: CustomAPIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const productId = v4();
        const product = await productService.createProduct({
            id: productId,
            ...<APIGatewayProductBodyObject>event.body,
            created_date: new Date().toISOString(),
        });
        return formatJSONResponse(<Response>{
            status: 200,
            data: { product },
            message: `Product - ${productId} created`
        });
    } catch (e) {
        return formatJSONResponse(<Response>{
            status: 500,
            data: {},
            message: getErrorMessage(e)
        });
    }
})

export const getAllProducts = middyfy(async (): Promise<APIGatewayProxyResult> => {
    try {
        const product = await productService.getAllProducts();
        return formatJSONResponse(<Response>{
            status: 200,
            data: { product },
            message: 'All Products retrieved'
        });
    } catch (e) {
        return formatJSONResponse(<Response>{
            status: 500,
            data: {},
            message: getErrorMessage(e)
        });
    }
})

export const updateProduct = middyfyWithUpsert(productSchema, async (event: CustomAPIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const id = event.pathParameters.id;
        const data = event.body;
        const product = await productService.updateProduct(id, data);
        return formatJSONResponse({
            status: 200,
            data: { product },
            message: `Product - ${id} updated`
        });
    } catch (e) {
        return formatJSONResponse(<Response>{
            status: 500,
            data: {},
            message: getErrorMessage(e)
        });
    }
})

export const deleteProduct = middyfy(async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const id = event.pathParameters.id;
        await productService.deleteProduct(id);
        return formatJSONResponse(<Response>{
            status: 200,
            data: {},
            message: `Product - ${id} deleted`
        });
    } catch (e) {
        return formatJSONResponse(<Response>{
            status: 500,
            data: {},
            message: getErrorMessage(e)
        });
    }
})