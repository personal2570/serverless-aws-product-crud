export type Response = {
    status: number;
    data: object;
    message: string;
}