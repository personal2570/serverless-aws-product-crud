export default interface Product {
    id: string;
    code: string;
    name: string;
    type: string;
    price: number;
    status: 'Active'|'Suspend'
    created_date: string;
}

export const productSchema = {
    type: 'object',
    properties: {
        body: {
        type: 'object',
        properties: {
            code: { type: 'string', minLength: 3, maxLength: 6 },
            name: { type: 'string' },
            type: { type: 'string' },
            price: { type: 'number', minimum: 1, },
            status: { type: 'string', pattern: '^(Active|Suspend)$'},
        },
        required: ['code', 'name', 'type', 'price', 'status']
        }
    }
};