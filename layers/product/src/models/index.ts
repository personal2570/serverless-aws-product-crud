import { DynamoDbHandler } from "../libs/dynamoDb";
import { DynamoDbClientHandler } from "../libs/dynamoDbClient";

const dynamoDbHandler = new DynamoDbClientHandler(process.env.PRODUCT_TABLE, DynamoDbHandler());

export default dynamoDbHandler;