import { DocumentClient } from "aws-sdk/clients/dynamodb";
import Product from "../models/product";

export class DynamoDbClientHandler {

    constructor(private tableName: string, private docClient: DocumentClient) {}

    async scan() {
        return await this.docClient.scan({
            TableName: this.tableName,
        }).promise();
    }

    async get(id: string) {
        return await this.docClient.get({
            TableName: this.tableName,
            Key: { id },
        })
        .promise();
    }

    async query(index: string, code: string) {
        return await this.docClient.query({
            TableName: this.tableName,
            ...this.__generateFilterIndexQuery(index, { code }),
        }).promise();
    }

    async put(product: Product) {
        return await this.docClient.put({
            TableName: this.tableName,
            Item: product
        }).promise();
    }

    async update(id: string, product: Partial<Product>) {
        return await this.docClient
            .update({
                TableName: this.tableName,
                Key: { id },
                ...this.__generateUpdateQuery(product)
            })
            .promise();
    }

    async delete(id: string) {
        return await this.docClient.delete({
            TableName: this.tableName,
            Key: { id }
        }).promise();
    }

    private __generateUpdateQuery(fields) {
        const exp = {
            UpdateExpression: 'set',
            ExpressionAttributeNames: {},
            ExpressionAttributeValues: {},
            ReturnValues: "ALL_NEW",
        };
        Object.entries(fields).forEach(([key, item]) => {
            exp.UpdateExpression += ` #${key} = :${key},`;
            exp.ExpressionAttributeNames[`#${key}`] = key;
            exp.ExpressionAttributeValues[`:${key}`] = item;
        })
        exp.UpdateExpression = exp.UpdateExpression.slice(0, -1);
        return exp;
    }

    private __generateFilterIndexQuery(index, fields) {
        const exp = {
            IndexName: index,
            KeyConditionExpression: '',
            ExpressionAttributeValues: {},
        };
        Object.entries(fields).forEach(([key, item]) => {
            exp.KeyConditionExpression += ` ${key} = :${key},`;
            exp.ExpressionAttributeValues[`:${key}`] = item;
        })
        exp.KeyConditionExpression = exp.KeyConditionExpression.slice(0, -1);
        return exp;
    }

}

