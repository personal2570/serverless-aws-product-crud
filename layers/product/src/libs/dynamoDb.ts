import * as AWS from "aws-sdk";

export function DynamoDbHandler() {

    if (process.env.IS_OFFLINE) {
        return new AWS.DynamoDB.DocumentClient({
            region: "localhost",
            endpoint: "http://localhost:5000",
        });
    } else {
        const dynamodb = new AWS.DynamoDB({ region: process.env.AWS_DYNAMODB_REGION })
        try {
            dynamodb.describeTable({ TableName: process.env.PRODUCT_TABLE }).promise();
        } catch (e) {
            throw `${process.env.PRODUCT_TABLE} Table not Exists`
        };
        return new AWS.DynamoDB.DocumentClient();
    }

}