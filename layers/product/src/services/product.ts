import { DynamoDbClientHandler } from "../libs/dynamoDbClient";
import Product from "../models/product";

export default class ProductService {

    private productCodeIndex = process.env.PRODUCT_TABLE_PRODUCT_CODE_INDEX;

    constructor(private DynamoDbClientHandler: DynamoDbClientHandler) { }

    async createProduct(product: Product): Promise<Product> {
        const isCodeExist = await this.DynamoDbClientHandler.query(this.productCodeIndex, product.code);
        if ( isCodeExist?.Count > 0 ) {
            throw `Product Code - ${product.code} exists`;
        }
        await this.DynamoDbClientHandler.put(product);
        return <Product>product;
    }

    async getAllProducts(): Promise<Product[]> {
        const products = await this.DynamoDbClientHandler.scan();
        return <Product[]>products.Items;
    }

    async updateProduct(id: string, product: Partial<Product>): Promise<Product> {
        const isProductExist = await this.DynamoDbClientHandler.get(id);
        if ( !isProductExist?.Item ) {
            throw `Product - ${id} not found`;
        }
        const updated = await this.DynamoDbClientHandler.update(id, product);
        return <Product>updated.Attributes;
    }

    async deleteProduct(id: string): Promise<any> {
        const isProductExist = await this.DynamoDbClientHandler.get(id);
        if ( !isProductExist?.Item ) {
            throw `Product - ${id} not found`;
        }
        return await this.DynamoDbClientHandler.delete(id);
    }

}