import dynamoDbHandler from "../models/index";
import ProductService from "./product"

const productService = new ProductService(dynamoDbHandler);

export default productService;