import type { AWS } from '@serverless/typescript';
import { createProduct, getAllProducts, updateProduct, deleteProduct } from '@functions/product';

const serverlessConfiguration: AWS = {
  service: 'product-crud',
  org: "${env:SERVERLESS_ORG}",
  app: "${env:SERVERLESS_APP}",
  frameworkVersion: '3',
  plugins: ['serverless-esbuild', 'serverless-offline', 'serverless-dynamodb-local'],
  useDotenv: true,
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    region: "ap-southeast-1",
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
      AWS_DYNAMODB_REGION: "${env:AWS_DYNAMODB_REGION}",
      PRODUCT_TABLE: "${env:PRODUCT_TABLE}",
      PRODUCT_TABLE_PRODUCT_CODE_INDEX: "${env:PRODUCT_TABLE_PRODUCT_CODE_INDEX}"
    },
    iam: {
      role: {
        statements: [{
          Effect: "Allow",
          Action: [
            "dynamodb:DescribeTable",
            "dynamodb:Query",
            "dynamodb:Scan",
            "dynamodb:GetItem",
            "dynamodb:PutItem",
            "dynamodb:UpdateItem",
            "dynamodb:DeleteItem",
          ],
          Resource: [
            "arn:aws:dynamodb:${env:AWS_DYNAMODB_REGION}:*:table/${env:PRODUCT_TABLE}",
            "arn:aws:dynamodb:${env:AWS_DYNAMODB_REGION}:*:table/${env:PRODUCT_TABLE}/index/${env:PRODUCT_TABLE_PRODUCT_CODE_INDEX}"
          ],
        }],
      },
    },
  },
  layers: {
    product: {
      path: './layers/product'
    },
    node: {
      path: './layers/nodejs'
    }
  },
  functions: { createProduct, getAllProducts, updateProduct, deleteProduct },
  package: { individually: true },
  custom:{
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
    dynamodb:{
      start:{
        port: 5000,
        inMemory: true,
        migrate: true,
      },
      stages: "dev"
    }
  },
  resources: {
    Resources: {
      ProductTable: {
        Type: "AWS::DynamoDB::Table",
        Properties: {
          TableName: "${env:PRODUCT_TABLE}",
          AttributeDefinitions: [
            {
              AttributeName: "id",
              AttributeType: "S",
            },
            {
              AttributeName: "code",
              AttributeType: "S",
            }
          ],
          KeySchema: [{
            AttributeName: "id",
            KeyType: "HASH"
          }],
          ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1
          },
          GlobalSecondaryIndexes: [{
            IndexName: "${env:PRODUCT_TABLE_PRODUCT_CODE_INDEX}",
            KeySchema: [{
              AttributeName: "code",
              KeyType: "HASH"
            }],
            Projection: {
              ProjectionType: "ALL"
            },
            ProvisionedThroughput: {
              ReadCapacityUnits: 1,
              WriteCapacityUnits: 1
            },
          }]
        }
      }
    }
  }
};
module.exports = serverlessConfiguration;